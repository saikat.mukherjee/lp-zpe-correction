#+TITLE: LP-ZPE correction module 
#+AUTHOR: Saikat Mukherjee <saikat.mukherjee@univ-amu.fr>

The Fortran module enables Local-Pair Zero-Point Energy (LP-ZPE) correction in the classical trajectories. The module can be interfaced with any classical trajectory propagator. 


## Description

The Fortran module enables Local-Pair Zero-Point Energy (LP-ZPE) correction
in the classical trajectories. The LP-ZPE method has been able to prevent the
ZPE spilling of the high frequency stretching modes (user defined, currently
implemented O-H and N-H stretching) by pumping back the leaked energy into
the corresponding modes while this energy is taken from the other modes of
the molecule itself, keeping the system as a microcanonical ensemble.

This module takes nuclear velocities as the main input and correct the velocities
if ZPE spilling is observed. This module can be interfaced with any classical
trajectory propagator. After determining the velocities by the propagator,
this module can be called for correcting the nuclear velocities as per LP-ZPE correction.

LP-ZPE correction method is described in the follwoing article:
Mukherjee, S.; Barbatti, M. A Hessian Free Method to Prevent Zero-Point Energy Leakage
in Classical Trajectory Simulation. ChemRxiv 2022. DOI: 10.26434/chemrxiv-2022-53g43


## Technical Details

At initialization of the dynamics, the local pairs need to be classified in two groups.
(a) The high-frequency modes built from the atom pairs, mainly prone to ZPE leakage
(such as hydride bonds), are named as AH bonds. These AH bonds will be monitored for
ZPE leakage, and if necessary, energy will be added to them. (b) All the other atom pairs,
not necessarily bonded, are named BC pairs. These atom pairs will donate energy to the
energy pool from where energy will be pumped to the leaked bond.

Few parameters are need to be fixed apriori.

  * kmodel         !! ZPE correction scheme (currently only one model is implemented).
                   !! value: 1

  * kcheck         !! Flag to trigger the ZPE corrections.
                   !! 0: Just check the values, do not correct anything.
                   !! 1: enables ZPE correction scheme (Input).

  * tcheck         !! Duration of time for checking ZPE leakage.
                   !! value: 10 fs

  * tcycle         !! ZPE correction will take place after every this time.
                   !! value: 10 fs

  * zthresh        !! Threshold value for allowed energy leakage.
                   !! value: 0.001 Hartree

  * biascorrect    !! Use bias correction while changing velocities.
                   !! logical: false


## Future Deveopments
